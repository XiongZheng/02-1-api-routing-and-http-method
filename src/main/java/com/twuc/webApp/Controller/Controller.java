package com.twuc.webApp.Controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    //2.1
    @GetMapping(value = "/api/users/{Id}/books")
    public String getBookId(@PathVariable Long Id) {
        return "The book for user " + Id;
    }

    //2.3
    @GetMapping("/api/segments/good")
    public String getGood() {
        return "good";
    }

    //2.3
    @GetMapping("/api/segments/{segmentName}")
    public String getSegments(@PathVariable String segmentName){
        return "segmentName";
    }

    //2。4
    @GetMapping("/api/?/test")
    public String getCharacter(){
        return "???";
    }

    //2.5
    @GetMapping("/api/wildcards/*")
    public String getSomething(){
        return "* match something";
    }

    //2.5
    @GetMapping("/api/wildcards/before/*/after")
    public String getMiddle(){
        return "* in the middle";
    }

    //2.6
    @GetMapping("/api/*/test/*")
    public String getMoreMiddleSignal(){
        return "more * in the url";
    }

    //2.7
    @GetMapping("/api/regex/^s/$d")
    public String getRegex(){
        return "regex can in the url";
    }

    //2.8
    @GetMapping("/api/students")
    @ResponseBody
    public String getGenderAndClass(@RequestParam String gender,@RequestParam(name = "class") String klass){
        return gender+"+"+klass;
    }
}
