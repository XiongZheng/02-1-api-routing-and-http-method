package com.twuc.webApp.ControllerTest;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    public MockMvc mockMvc;

    //2.1
    @Test
    void should_return_match_id2() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(content().string("The book for user 2"));
    }

    //2.1
    @Test
    void should_return_match_id23() throws Exception {
        mockMvc.perform(get("/api/users/23/books"))
                .andExpect(content().string("The book for user 23"));
    }

    //2.3
    @Test
    void should_distinct_good() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("good"));
    }

    //2.3
    @Test
    void should_distinct_segments() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("good"));
    }

    //2.4
    @Test
    void should_return_one_character() throws Exception {
        mockMvc.perform(get("/api/s/test"))
                .andExpect(status().is(200));
    }

    //2.4
    @Test
    void should_return_more_character() throws Exception {
        mockMvc.perform(get("/api/sss/test"))
                .andExpect(status().is(404));
    }

    //2.4
    @Test
    void should_return_null_character() throws Exception {
        mockMvc.perform(get("/api//test"))
                .andExpect(status().is(404));
    }

    //2.5
    @Test
    void should_match_everything() throws Exception {
        mockMvc.perform(get("/api/wildcards/*"))
                .andExpect(status().isOk());
    }

    //2.5
    @Test
    void should_match_something() throws Exception {
        mockMvc.perform(get("/api/wildcards/something"))
                .andExpect(status().isOk());
    }

    //2.5
    @Test
    void should_match_middle() throws Exception {
        mockMvc.perform(get("/api/wildcards/before/middle/after"))
                .andExpect(status().isOk());
    }

    //2.5
    @Test
    void should_match_middle_is_null() throws Exception {
        mockMvc.perform(get("/api/wildcards/before/after"))
                .andExpect(status().isNotFound());
    }

    //2.6
    @Test
    void should_write_signal_in_middle_url() throws Exception {
        mockMvc.perform(get("/api/start/test/end"))
                .andExpect(status().isOk());
    }

    //2.7
    @Test
    void should_write_regex_in_url() throws Exception {
        mockMvc.perform(get("/api/regex/start/end"))
                .andExpect(status().isNotFound());
    }

    //2.8
    @Test
    void should_get_RequestParam() throws Exception {
        mockMvc.perform(get("/api/students?gender=female&class=A"))
                .andExpect(content().string("female+A"));
    }

    //2.8
    @Test
    void should_work_without_RequestParam() throws Exception {
        mockMvc.perform(get("/api/students"))
                .andExpect(status().isBadRequest());
    }
    
    //2.8
    @Test
    void should_not_present() throws Exception {
        mockMvc.perform(get("/api/students?gender=female&klass=A"))
                .andExpect(status().isBadRequest());
    }
}
